
package com.example.openweatherapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rain {

    @SerializedName("1h")
    @Expose
    private Integer _1h;

    public Integer get1h() {
        return _1h;
    }

    public void set1h(Integer _1h) {
        this._1h = _1h;
    }

}
